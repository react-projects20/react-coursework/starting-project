import { useState } from 'react';
import reactImg from './assets/react-core-concepts.png';
import componentImg from './assets/components.png';
import TestExample from './Test';
import { CORE_CONCEPTS, EXAMPLES } from './data.js';
import TabButton from './TabButton.jsx';

const reactDescriptions = ['Fundamental', 'Crucial', 'Core'];

function genRandomInt(max) {
  return Math.floor(Math.random() * (max + 1));
}

function Header() {
  const description = reactDescriptions[genRandomInt(2)]

  return (
    <header>
      <img src={reactImg} alt="Stylized atom" />
      <h1>React Essentials</h1>
      <p>
        {description} React concepts you will need for almost any app you are
        going to build!
      </p>
    </header>
  )
}

//object destructering so no longer do you have to use props.title or props.image or props.description
//because we have listed our the props  in {} and said that title, image and description should be used from props etc...
function CoreConcept({title, image, description}) {
  return (
    <li className='card'>
      <img src={image} alt={title} />
      <h3>{title}</h3>
      <p>{description}</p>
    </li>
  )

}


//These are custom properties where i am passing from App function to CoreConept function
//Test example os where i am passing from App function to testexample function which is in another file
function App(props) {

  //must be called at the top level and not nested in if/for loop statements
  const [selectedTop, setSelectedTop] = useState(null);


//For clicks we need to create a function for it
function handleClick(selectedButton){
  setSelectedTop(selectedButton)
  
}

//This is another way to display for rendering content  based on state


// let tabContent = <p>Please select a topic</p>

// if (selectedTop) {
//   tabContent = (
//     <div id='tab-content'>
//             <h3>
//               {EXAMPLES[selectedTop].title}
//             </h3>
//             <p>{EXAMPLES[selectedTop].description}</p>
//             <pre>
//               <code>
//               {EXAMPLES[selectedTop].code}
//               </code>
//             </pre>
//           </div>
//   )
// }

  return (
    <div>
      <Header />
      <main>
        <section id="core-concepts">
          <h2>Core Concepts</h2>
            <ul>
              {CORE_CONCEPTS.map((conceptItem) => (
                <CoreConcept key={conceptItem.title} {...conceptItem}/>
                ))}
              
              
              {/* We ne no longer need to manually output data using this way */}
              {/* <CoreConcept title={CORE_CONCEPTS[0].title} 
                description={CORE_CONCEPTS[0].description} 
                image={CORE_CONCEPTS[0].image} 
              />
              <CoreConcept {...CORE_CONCEPTS[1]}/>
              <CoreConcept {...CORE_CONCEPTS[2]}/>
              <CoreConcept {...CORE_CONCEPTS[3]}/> */}
            </ul>
          
        </section>
        
        <section id="examples">
          <h2>Time to get started!</h2>
          <menu>
            <TabButton isActive={selectedTop === 'components'} label="Components" onClick={() => handleClick('components')}>Comp</TabButton>
            <TabButton isActive={selectedTop === 'jsx'} label="JSX" onClick={() => handleClick('jsx')}>Comp</TabButton>
            <TabButton isActive={selectedTop === 'props'} label="Props" onClick={() => handleClick('props')}>Comp</TabButton>
            <TabButton isActive={selectedTop === 'state'} label="State" onClick={() => handleClick('state')}>Comp</TabButton>
            
          </menu>

          {!selectedTop && <p>Please select a topic</p>}
          {selectedTop ? <div id='tab-content'>
            <h3>
              {EXAMPLES[selectedTop].title}
            </h3>
            <p>{EXAMPLES[selectedTop].description}</p>
            <pre>
              <code>
              {EXAMPLES[selectedTop].code}
              </code>
            </pre>
          </div> : null}
          
          {/* {tabContent} */}

        </section>
        {/* <TestExample justSomething="testfromApps" />
        <TestExample mynew="comes from this">{props.cool}</TestExample> */}
      </main>
    </div>
  );
}

export default App;
