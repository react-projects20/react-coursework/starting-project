function TabButton({label, isActive, ...props}) {
    
    
    
    //in the onClick props just pass the function without parathesis
    return (
        <div>
            <button className={isActive ? 'active': undefined} {...props}>{label}</button>
        </div>
    
            
        
    )
}

export default TabButton;